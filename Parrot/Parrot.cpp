#include "Parrot.h"

Parrot::Parrot(Type type, int numberOfCoconuts, double voltage, bool isInjured) :
    m_type(type), m_numberOfCoconuts(numberOfCoconuts), m_voltage(voltage), m_isInjured(isInjured)
{

}

double Parrot::GetSpeed() const
{
    switch (m_type)
    {
    case Type::EUROPEAN:
        return GetBaseSpeed();
    case Type::AFRICAN:
        return std::max(0., GetBaseSpeed() - GetLoadFactor() * m_numberOfCoconuts);
    case Type::NORWEGIAN_BLUE:
        return m_isInjured ? 0. : GetBaseSpeed(m_voltage);
    }

    throw std::logic_error("Should be unreachable");
}

Parrot::Type Parrot::GetType() const
{
    return m_type;
}

double Parrot::GetBaseSpeed(double voltage) const
{

    return std::min(24., voltage * GetBaseSpeed());
}

double Parrot::GetBaseSpeed() const
{
    return 12.;
}

double Parrot::GetLoadFactor() const
{
    return 9.;
}

Parrot* CreateParrot(Parrot::Type type, int numberOfCoconuts, double voltage, bool isInjured)
{
    return new Parrot(type, numberOfCoconuts, voltage, isInjured);
}
