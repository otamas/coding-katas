Shop refactoring kata

This kata is a slightly modified version of the Gilded Rose Refactoring Kata with 
a simple unit test coverage and slightly modified structure

The Gilded Rose Kata was originally created by Terry Hughes (http://twitter.com/#!/TerryHughes). Thanks!

The idea is not to re-write the code from scratch, but rather to practice designing tests, 
taking small steps, running the tests often, and incrementally improving the design.

Please read the requirements.txt to get going.
FYI, Find attached the original Gilded Rose requirements (gilded_rose_requirements.txt)
